package malaga.teleco.puertasabiertas;


import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.widget.ProgressBar;
import android.widget.TextView;

public class PuertasAbiertasActivity extends Activity {
	private ProgressBar pb;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		pb = (ProgressBar) findViewById(R.id.progressBar1);

		pb.setMax(100);

		rellenaBarra b = new rellenaBarra();
		b.execute();

		TextView t1 = (TextView) findViewById(R.id.texto1);
		TextView t2 = (TextView) findViewById(R.id.texto2);

		t1.setText(getString(R.string.hello).replace("_", getString(R.string.nombre)));
		t2.setText(getString(R.string.convence).replace("_", getString(R.string.nombre)));
		
		
		
	}

	private void setBarra(int i) {
		pb.setProgress(i);
	}

	private int getBarra() {
		return pb.getProgress();
	}

	private class rellenaBarra extends AsyncTask<Void, Integer, Void> {
		int myProgress;

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			myProgress = 0;
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			while (myProgress < 100) {
				myProgress++;
				publishProgress(myProgress);
				SystemClock.sleep(100);
				
			
				
			}
			
			Intent i = new Intent(PuertasAbiertasActivity.this,
					gradosViewPagerActivity.class);
			startActivity(i);
			return null;
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			// TODO Auto-generated method stub
			pb.setProgress(values[0]);
		}
	}
}