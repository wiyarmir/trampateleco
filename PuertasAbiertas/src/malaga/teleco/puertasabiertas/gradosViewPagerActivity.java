package malaga.teleco.puertasabiertas;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v4.view.ViewPager.SimpleOnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class gradosViewPagerActivity extends Activity {
	private ViewPager columnas;
	private static int NUM_COLUMNAS = 5;
	private Context cxt;
	private ColumnasAdapter miAdapter;
	private TextView titulo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		super.onCreate(savedInstanceState);
		setContentView(R.layout.grados);
		cxt = this;
		titulo = (TextView) findViewById(R.id.titulo);
		
		miAdapter = new ColumnasAdapter();
		columnas = (ViewPager) findViewById(R.id.viewpager);
		columnas.setAdapter(miAdapter);
		
		columnas.setOnPageChangeListener(new OnPageChangeListener() {
			
			public void onPageSelected(int arg0) {
				
				if (arg0 == 0){
					titulo.setText("Grado Generalista");
					
				}else if (arg0 == 1){
					titulo.setText("Sistemas Electronicos");
				} else if (arg0 == 2){
					titulo.setText("Sistemas Sonido e Imagen");
				}else if (arg0 == 3){
					titulo.setText("telematica");
				}else {
					titulo.setText("Sistemas De telecomunicacion");
				}
				
			}
			
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
				
			}
			
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		

	}
	
	

	

	private class ColumnasAdapter extends PagerAdapter {

		@Override
		public void destroyItem(View collection, int arg1, Object view) {
			((ViewPager) collection).removeView((LinearLayout) view);

		}
		
		

		@Override
		public void finishUpdate(View arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == ((LinearLayout) object);

		}

		@Override
		public void restoreState(Parcelable arg0, ClassLoader arg1) {
			// TODO Auto-generated method stub

		}

		@Override
		public Parcelable saveState() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void startUpdate(View arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public int getCount() {
			return NUM_COLUMNAS;
		}

		@Override
		public Object instantiateItem(View collection, int position) {
			LinearLayout v = (LinearLayout) LayoutInflater.from(cxt).inflate(
					R.layout.generalista, null);

			if (position == 0) {
				v = (LinearLayout) LayoutInflater.from(cxt).inflate(
						R.layout.generalista, null);

			} else if (position == 1) {

				v = (LinearLayout) LayoutInflater.from(cxt).inflate(
						R.layout.sistemaselectronicos, null);

			} else if (position == 2) {

				v = (LinearLayout) LayoutInflater.from(cxt).inflate(
						R.layout.sonidoimagen, null);

			} else if (position == 3) {

				v = (LinearLayout) LayoutInflater.from(cxt).inflate(
						R.layout.telematica, null);

			} else {

				v = (LinearLayout) LayoutInflater.from(cxt).inflate(
						R.layout.sistemasdeteleco, null);
			}

			((ViewPager) collection).addView(v, 0);

			return v;
		}

	}

}
